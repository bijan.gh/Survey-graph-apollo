## Production

- Run `npm run build`
- The above command produce and index.js file in dist folder you can use it
- Run `npm run server:prod`

---

## Development

- Just run `npm run server:dev`
