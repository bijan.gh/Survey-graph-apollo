import { ApolloServer, gql } from "apollo-server";
import typeDefs from "./schema/index";
import resolvers from "./resolvers/index";
import * as db from "./utils/db";
import * as models from "./models/index";

// connect to database
db.connect();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: () => {
    return {
      models
    };
  }
});

server.listen().then(({ url }) => console.log(`Server ready at ${url}`));
