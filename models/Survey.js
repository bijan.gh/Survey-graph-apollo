import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Survey = new Schema(
  {
    text: { type: String },
    isActive: { type: Boolean },
    created_at: { type: Number, required: true },
    applications: [{ type: Schema.Types.ObjectId, ref: "Application" }],
    respondents_answers: [
      {
        respondent: { type: Schema.Types.ObjectId, ref: "Respondent" },
        survey_answer: { type: Schema.Types.ObjectId, ref: "SurveyAnswer" }
      }
    ],
    steps: [
      {
        title: { type: String },
        applications: [{ type: Schema.Types.ObjectId, ref: "Application" }],
        questions: [
          {
            type: Schema.Types.ObjectId,
            ref: "Question",
            default: []
          }
        ],
        buttonText: { type: String }
      }
    ]
  },
  {
    collection: "surveys"
  }
);

export default mongoose.model("Survey", Survey);
