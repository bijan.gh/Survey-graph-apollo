import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Respondent = new Schema(
  {
    full_name: {
      type: String,
      default: ""
    },
    email: {
      type: String,
      unique: true
    },
    ip: {
      type: String
    },
    location: {
      type: Schema.Types.Mixed
    },
    surveys_answered: [
      {
        survey_answer: { type: Schema.Types.ObjectId, ref: "SurveyAnswer" },
        survey: { type: Schema.Types.ObjectId, ref: "Survey" }
      }
    ],
    created_at: { type: Number, required: true }
  },
  {
    collection: "respondents"
  }
);

export default mongoose.model("Respondent", Respondent);
