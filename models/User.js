import mongoose from "mongoose";
const Schema = mongoose.Schema;

const User = new Schema(
  {
    full_name: {
      type: String,
      default: ""
    },
    email: {
      type: String,
      unique: true
    },
    image: {
      type: String,
      default: ""
    },
    password: {
      type: String
    },
    role: {
      type: String,
      enum: ["superadmin", "admin", "user"]
    }
  },
  {
    collection: "users"
  }
);

export default mongoose.model("User", User);
