import mongoose from "mongoose";
const Schema = mongoose.Schema;

const SurveyAnswer = new Schema(
  {
    created_at: { type: Number, required: true },
    survey: { type: Schema.Types.ObjectId, ref: "Survey" },
    respondent: { type: Schema.Types.ObjectId, ref: "Respondent" },
    where: { type: String },
    url: { type: String, default: "" },
    answers: [
      {
        question: { type: Schema.Types.ObjectId, ref: "Question" },
        answers: [{ type: Schema.Types.ObjectId }]
      }
    ]
  },
  {
    collection: "survey-answers"
  }
);

export default mongoose.model("SurveyAnswer", SurveyAnswer);
