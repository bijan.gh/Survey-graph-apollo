import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Cron = new Schema(
  {
    created_at: { type: Number, required: true }
  },
  {
    collection: "crons"
  }
);

export default mongoose.model("Cron", Cron);
