import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Analytic = new Schema(
  {
    created_at: { type: Number, required: true },
    survey: { type: Schema.Types.ObjectId, ref: "Survey" },
    where: { type: String },
    url: { type: String, default: "" },
    question: { type: Schema.Types.ObjectId, ref: "Question" },
    question_text: { type: String },
    answer: { type: Schema.Types.ObjectId },
    answer_text: { type: String },
    respondent: { type: Schema.Types.ObjectId, ref: "Respondent" },
    respondent_location: {
      type: Schema.Types.Mixed
    },
    respondent_email: { type: String }
  },
  {
    collection: "analytics"
  }
);

export default mongoose.model("Analytic", Analytic);
