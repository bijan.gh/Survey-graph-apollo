import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Application = new Schema(
  {
    name: {
      type: String,
      default: ""
    }
  },
  {
    collection: "applications"
  }
);

export default mongoose.model("Application", Application);
