import mongoose from "mongoose";
const Schema = mongoose.Schema;
const Question = new Schema(
  {
    text: { type: String },
    isMulti: { type: Boolean, default: false },
    applications: [{ type: Schema.Types.ObjectId, ref: "Application" }],
    answers: [
      {
        text: { type: String },
        respondents: [
          { type: Schema.Types.ObjectId, ref: "Respondent", default: [] }
        ]
      }
    ],
    respondents: [
      { type: Schema.Types.ObjectId, ref: "Respondent", default: [] }
    ]
  },
  {
    collection: "questions"
  }
);

export default mongoose.model("Question", Question);
