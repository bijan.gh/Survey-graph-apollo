const survey = async (parent, args, context, info) => {
  const {
    models: { Survey }
  } = context;
  const { _id } = args;

  // set filter args to context
  context.surveyArgs = args;

  return await Survey.findOne({ _id });
};

const respondentsCount = async (parent, args, context, info) => {
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id };
  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.count(query);
};

const emails = async (parent, args, context, info) => {
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id };
  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.find(query).distinct("respondent_email");
};

const questions = async (parent, args, context, info) => {
  const {
    models: { Analytic, Question },
    surveyArgs: { _id }
  } = context;

  const questionIds = await Analytic.distinct("question", {
    survey: _id
  });

  return await Question.find({ _id: { $in: questionIds } }).lean();
};

const applications = async (parent, args, context, info) => {
  const {
    models: { Analytic, Application },
    surveyArgs: { _id }
  } = context;

  let apps = await Analytic.distinct("where", { survey: _id });
  return await Application.find({ name: { $in: apps } }).lean();
};

export default {
  Query: {
    survey
  },
  Survey: {
    respondentsCount,
    emails,
    questions,
    applications
  }
};
