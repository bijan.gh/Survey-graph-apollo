const emails = async (parent, args, context, info) => {
  const { _id: questionId } = parent;
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id, question: questionId };

  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.find(query).distinct("respondent_email");
};

const respondentsCount = async (parent, args, context, info) => {
  const { _id: questionId } = parent;
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id, question: questionId };

  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.count(query);
};

const answers = async (parent, args, context, info) => {
  const { _id } = parent;
  const {
    models: { Question }
  } = context;

  let question = await Question.findOne({ _id });
  return question.answers;
};

export default {
  Question: {
    emails,
    respondentsCount,
    answers
  }
};
