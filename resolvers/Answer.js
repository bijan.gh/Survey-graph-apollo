const emails = async (parent, args, context, info) => {
  const { _id: answerId } = parent;
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id, answer: answerId };

  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.find(query).distinct("respondent_email");
};

const respondentsCount = async (parent, args, context, info) => {
  const { _id: answerId } = parent;
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id, answer: answerId };

  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.count(query);
};

export default {
  Answer: {
    emails,
    respondentsCount
  }
};
