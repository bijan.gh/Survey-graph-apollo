const emails = async (parent, args, context, info) => {
  const { name } = parent;
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id, where: name };

  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.find(query).distinct("respondent_email");
};

const respondentsCount = async (parent, args, context, info) => {
  const { name } = parent;
  const {
    models: { Analytic },
    surveyArgs: { _id, startTime, endTime }
  } = context;

  let query = { survey: _id, where: name };

  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  return await Analytic.count(query);
};

export default {
  Application: {
    emails,
    respondentsCount
  }
};
