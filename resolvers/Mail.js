import { ApolloError } from "apollo-server";
import nodemailer from "nodemailer";

const filterMail = async (parent, args, context, info) => {
  const {
    models: { Analytic }
  } = context;
  const { surveyId, answersIds, startTime, endTime, applications } = args;
  let query = { survey: surveyId };
  if (startTime && endTime) {
    query.created_at = {
      $gte: startTime,
      $lte: endTime
    };
  }

  if (answersIds && answersIds.length !== 0) {
    query.answer = { $in: answersIds };
  }

  if (applications && applications.length !== 0) {
    query.where = { $in: applications };
  }

  return await Analytic.find(query).distinct("respondent_email");
};

const sendMail = async (
  parent,
  { emails, html, subject },
  context,
  info
) => {
  if (!emails || emails.length === 0)
    return new ApolloError("At least one email required");

  const options = {
    service: "SendGrid",
    auth: {
      user: "pixflow",
      pass: "joe09397506261"
    }
  };

  const transporter = nodemailer.createTransport(options);

  let mailInfo = await transporter.sendMail({
    from: "Motion Factory Team<hello@pixflow.net>",
    to: emails.join(","),
    subject,
    html
  });

  return mailInfo.accepted;
};

export default {
  Query: {
    filterMail
  },
  Mutation: {
    sendMail
  }
};
