import { merge } from "lodash";
import Survey from "./Survey";
import Application from "./Application";
import Question from "./Question";
import Answer from "./Answer";
import Mail from "./Mail";

export default merge({}, Survey, Application, Question, Answer, Mail);
