import Survey from "./Survey";
import Question from "./Question";
import Application from "./Application";
import Answer from "./Answer";
import Mail from "./Mail";

const Query = `
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }
`;

export default [Query, Survey, Question, Application, Answer, Mail];
