import { gql } from "apollo-server";

export default gql`
  type Answer {
    _id: ID
    text: String
    emails: [String]
    respondentsCount: Int
  }
`;
