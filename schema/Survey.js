import { gql } from "apollo-server";

export default gql`
  type Survey {
    _id: ID
    text: String
    isActive: Boolean
    respondentsCount: Int
    emails: [String]
    questions: [Question]
    applications: [Application]
  }

  extend type Query {
    survey(_id: ID!, startTime: String, endTime: String): Survey
  }
`;
