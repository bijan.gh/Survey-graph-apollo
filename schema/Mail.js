import { gql } from "apollo-server";

export default gql`
  extend type Query {
    filterMail(
      surveyId: ID!
      answersIds: [ID!]
      applications: [String!]
      startTime: String
      endTime: String
    ): [String]
  }

  extend type Mutation {
    sendMail(emails: [String!]!, subject: String!, html: String!): [String]
  }
`;
