import { gql } from "apollo-server";

export default gql`
  type Question {
    _id: ID
    text: String
    isMulti: Boolean
    emails: [String]
    respondentsCount: Int
    answers: [Answer]
  }
`;
