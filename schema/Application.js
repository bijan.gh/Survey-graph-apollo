import { gql } from "apollo-server";

export default gql`
  type Application {
    _id: ID
    name: String
    emails: [String]
    respondentsCount: Int
  }
`;
