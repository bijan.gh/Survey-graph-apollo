import mongoose from "mongoose";
import { config } from "dotenv";

// load env
config();

const { DATABASE_NAME } = process.env;

export const connect = () => {
  mongoose.connect(`mongodb://localhost/${DATABASE_NAME}`);

  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => console.log("Connected to database"));
};
